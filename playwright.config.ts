/* TODO:
            -Add Test Step As Decorator +-
            -Add Actions -+
            -Add API Calls -
            -Add Readme -
            -Add logger -
            -Add routing -
            -CI Integration +-
            -Testrail Integration -
            -Rename Element Handler -
            -Add Request Interceptors -
*/
import { PlaywrightTestConfig } from '@playwright/test';

const playWrightConfig: PlaywrightTestConfig = {
  use: {
    trace: 'on-first-retry',
    defaultBrowserType: 'chromium',
    screenshot: 'only-on-failure',
    headless: Boolean(process.env.CI),
    viewport: {
      width: 1920,
      height: 1080,
    },
    ignoreHTTPSErrors: true,
    video: 'off',
  },
  retries: 1,
  reporter: [
    ['line'],
    ['allure-playwright'],
    ['html', { open: 'never' }],
  ],
};
export default playWrightConfig;
