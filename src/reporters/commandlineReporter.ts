import {
  FullConfig,
  FullResult,
  Reporter,
  Suite,
  TestCase,
  TestError,
  TestResult,
  TestStep,
} from '@playwright/test/reporter';
import { chalkString } from '../helpers/chalkUtils';
import { ChalkColors } from '../enums/chalkColors';

//TODO: IMPLEMENT CLI REPORTER

class CommandLineReporter implements Reporter {
  onBegin(config: FullConfig, suite: Suite) {
    chalkString(`Starting the run with ${suite.allTests().length} tests`, ChalkColors.yellow);
  }

  onTestBegin(test: TestCase) {
    chalkString(`Starting test ${test.title}`, ChalkColors.yellow);
  }

  onStepEnd(test: TestCase, result: TestResult, step: TestStep) {
    chalkString(`${test.title}::Step ended '${step.title}', Duration: [${step.duration}] ms`, ChalkColors.yellow);
  }

  onTestEnd(test: TestCase, result: TestResult) {
    chalkString(`Finished test ${test.title}: ${result.status}`, ChalkColors.yellow);
  }

  onEnd(result: FullResult): void | Promise<void> {
    chalkString(`Finished the run: ${result.status}`, ChalkColors.yellow);
  }

  onError(error: TestError) {
    chalkString(`Test error: ${error}`, ChalkColors.yellow);
  }

  onStepBegin(test: TestCase, result: TestResult, step: TestStep) {
    chalkString(`${test.title}::Step started '${step.title}`, ChalkColors.yellow);
  }
}

export default CommandLineReporter;
