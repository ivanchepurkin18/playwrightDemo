import test from '../services/fixtures/loginFixture';
import { expect, Page } from '@playwright/test';
import { IElement } from './elementHandler';

//TODO: ADD ALL NEEDED ASSERTIONS

export class Assertions {
  static async expectToBe<T>(actual: T, expected: T): Promise<void> {
    await test.step(`Expect Actual result "${actual}" to Be equal with Expected ${expected}`, async () => {
      await expect(actual).toBe(expected);
    });
  }

  static async expectToBeVisible(element: IElement): Promise<void> {
    await test.step(`Expect "${element.description}" element to Be Visible`, async () => {
      await expect(element).toBeVisible();
    });
  }

  static async expectToBeHidden(element: IElement): Promise<void> {
    await test.step(`Expect "${element.description}" element to Be Hidden`, async () => {
      await expect(element).toBeHidden();
    });
  }

  static async expectElementToHaveText(element: IElement, text: string | RegExp | (string|RegExp)[]): Promise<void> {
    await test.step(`Expect "${element.description}" element to have text: "${text}"`, async () => {
      await expect(element).toHaveText(text);
    });
  }

  static async expectPageUrlToBe(page: Page, url: string|RegExp): Promise<void> {
    await test.step(`Expect "${await page.title()}" page to have url: "${url}"`, async () => {
      await expect(page).toHaveURL(url);
    });
  }
}
