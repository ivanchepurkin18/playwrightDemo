export class StringUtils {
  static replaceSpacesFromString(string: string): string {
    return string.replace(/\s/g, '');
  }

  static capitalizeFirstLetter(string: string): string {
    return string
      .toString()
      .charAt(0)
      .toUpperCase() + string.slice(1);
  }
}
