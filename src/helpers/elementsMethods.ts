import { IElement } from './elementHandler';

export class ElementsMethods {
  static async getTextFromElement(element: IElement): Promise<string|null> {
    return await element.textContent();
  }

  static getTextFromElementsArr(element: IElement): Promise<Array<string>> {
    return element.allTextContents();
  }
}
