import chalk from 'chalk';
import { ChalkColors } from '../enums/chalkColors';

export function chalkString(string: string, color: ChalkColors) {
  return chalk[color](string);
}
