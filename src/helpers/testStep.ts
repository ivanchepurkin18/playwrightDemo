import { test } from '@playwright/test';

//TODO: wait till custom ts config will be available, and use step as decorator

export function testStep(message: string) {
  return (target: Object, property: string, descriptor: PropertyDescriptor) => {
    const originalFunction = descriptor.value;

    descriptor.value = function (...args: any[]) {
      return test.step(
        message,
        () => originalFunction.apply(this, args),
      );
    };
    return descriptor;
  };
}
