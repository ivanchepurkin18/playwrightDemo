import test from '../services/fixtures/loginFixture';
import { IElement } from './elementHandler';
import { KeyboardEventNames } from '../enums/keyboardEventNames';
import { AttributeNames } from '../enums/attributeNames';

interface ITypeOptions {
    force?: boolean,
    noWaitAfter?: boolean,
    timeout?: number,
    withClear?: boolean
}

interface IPressOptions {
    delay?: number,
    noWaitAfter?: boolean,
    timeout?: number
}

interface IUploadOptions {
    noWaitAfter?: boolean,
    timeout?: number
}

export class Actions {
  public static async click(element: IElement): Promise<void> {
    await test.step(`Click on: "${element.description}" Element`, async () => {
      await element.click();
    });
  }

  public static async type(element: IElement, text: string, options?: ITypeOptions): Promise<void> {
    await test.step(`Type text: "${text}" to "${element.description}" Element`, async () => {
      if (options.withClear) {
        await Actions.clearInput(element);
      }
      await element.fill(text, options);
    });
  }


  static async clearInput(element: IElement): Promise<void> {
    if (await element.getAttribute(AttributeNames.value)) {
      await Actions.pressButton(element, KeyboardEventNames.backSpace);
      await this.clearInput(element);
    }
  }

  static async pressButton(element: IElement, button: KeyboardEventNames, options?: IPressOptions): Promise<void> {
    await test.step(`Press "${button}" Keyboard button`, async () => {
      await element.press(button, options);
    });
  }

  static async uploadFile(element: IElement, pathToFile: string| string[], options?: IUploadOptions): Promise<void> {
    await test.step(`Upload File to input: "${element.description}"`, async () => {
      await element.setInputFiles(pathToFile, options);
    });
  }
}
