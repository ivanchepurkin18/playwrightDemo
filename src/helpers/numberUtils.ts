export class NumberUtils {
  static generateRandomNumber(stringLength: number): number {
    return Math.floor(Math.random() * (9 * Math.pow(10, stringLength - 1))) + Math.pow(10, stringLength - 1);
  }
}
