import { Page } from '@playwright/test';
import { RequestInterceptionError } from '../../errors/requestInterceptorError';
import { Response } from 'playwright-core';

interface IApiData {
    requestData: {
        requestBody: object|null,
    },
    responseData: {
        responseBody?,
        statusCode: number,
    },
}

export class RequestInterceptor {
  protected readonly page: Page;

  constructor(page: Page) {
    this.page = page;
  }

  public async getAPIData(endpoint: string, method: string): Promise<IApiData> {
    const response: Response = await this.page.waitForResponse(response => {
      const checkResult = response.url().endsWith(endpoint) && response.request().method() === method;
      if (!checkResult) {
        throw new RequestInterceptionError(
          `Request with endpoint: "${endpoint}", and method: "${method}" was not done`,
        );
      }
      return checkResult;
    }, { timeout: 5000 });
    return {
      requestData: {
        requestBody: JSON.parse(response.request().postData()),
      },
      responseData: {
        responseBody: await response.text() ? await response.json() : undefined,
        statusCode: response.status(),
      },
    };
  }
}
