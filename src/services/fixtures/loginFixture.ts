import { test as base } from '@playwright/test';
import { BasePage } from '../../pages/basePage';

type TestFixtures = {
    login: void
}

const test = base.extend<TestFixtures>({
  async login ({ page }, use) {
    await use(await new BasePage(page).navigate('https://playwright.dev/docs/test-fixtures'));
  },
});

export default test;
